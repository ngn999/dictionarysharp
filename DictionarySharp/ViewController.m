//
//  ViewController.m
//  DictionarySharp
//
//  Created by ngn999 on 16/3/30.
//  Copyright © 2016年 ngn999. All rights reserved.
//

#import "ViewController.h"
#import "Masonry/Masonry.h"
#import "ReactiveCocoa/ReactiveCocoa.h"

@interface ViewController () <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UISearchBar *searchBar;
// table
@property (nonatomic, strong) UITextChecker *textChecker;
@property (atomic, strong) NSMutableArray *words;
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.textChecker = [[UITextChecker alloc] init];
    self.words = [[NSMutableArray alloc] init];
    
    self.searchBar = [[UISearchBar alloc] init];
    self.searchBar.placeholder = @"搜索";
    self.searchBar.showsCancelButton = YES;
    [self.view addSubview:self.searchBar];
    self.navigationItem.titleView = self.searchBar; // 把searchbar放到navigator里面
    @weakify(self);
    // 返回类型是 RACTuple
    [[self rac_signalForSelector:@selector(searchBarSearchButtonClicked:) fromProtocol:@protocol(UISearchBarDelegate)]
     subscribeNext:^(RACTuple *x) {
         @strongify(self);
         UISearchBar *bar = x.first;
         // NSLog(@"=%@=", bar.text);
         [self searchDictionaryWithText:bar.text];
     }];
    
    [[self rac_signalForSelector:@selector(searchBarCancelButtonClicked:) fromProtocol:@protocol(UISearchBarDelegate)] subscribeNext:^(id x) {
        @strongify(self);
        [self.searchBar resignFirstResponder];
        self.searchBar.showsCancelButton = NO;
    }];
    
    [[self rac_signalForSelector:@selector(searchBarTextDidBeginEditing:) fromProtocol:@protocol(UISearchBarDelegate)]
     subscribeNext:^(id x) {
         @strongify(self);
         self.searchBar.showsCancelButton = YES;
     }];
    
    // 拿suggest
    RACSignal *textSignal =  [[[[[[self rac_signalForSelector:@selector(searchBar:textDidChange:) fromProtocol:@protocol(UISearchBarDelegate)]
                              map:^id(RACTuple *value) {
                                  NSString *text = value.second;
                                  return text;
                              }] filter:^BOOL(NSString *value) {
                                  return value != nil;
                              }] flattenMap:^RACStream *(id value) {
                                  @strongify(self);
                                  return [self suggestSiganl];
                              }] deliverOn:[RACScheduler schedulerWithPriority:RACSchedulerPriorityBackground]] flattenMap:^RACStream *(NSArray *words) {
                                  return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
                                      NSArray *res = [[[words rac_sequence] filter:^BOOL(NSString *value) {
                                          return [UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:value];
                                      }] array];
                                      [subscriber sendNext:res];
                                      return nil;
                                  }];
                              }];
    RAC(self, words) = textSignal;
    [[RACObserve(self, words) deliverOn:[RACScheduler mainThreadScheduler]]subscribeNext:^(id x) {
        @strongify(self);
        // NSLog(@"|||%@|||", self.words);
        [self.tableView reloadData];
    }];
    
    self.searchBar.delegate = nil;
    self.searchBar.delegate = self;
    
    // 下拉提示
    self.tableView = [[UITableView alloc] init];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [[self rac_signalForSelector:@selector(tableView:didSelectRowAtIndexPath:) fromProtocol:@protocol(UITableViewDelegate)]
     subscribeNext:^(RACTuple *value) {
         @strongify(self);
         NSIndexPath *index = value.second;
         [self searchDictionaryWithText:[self.words objectAtIndex:index.row]];
     }];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.searchBar becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.searchBar becomeFirstResponder];
}

- (void) searchDictionaryWithText:(NSString *)text {
    if (!text || [text isEqualToString:@""]) return;
    [self.searchBar resignFirstResponder];
    UIReferenceLibraryViewController *referenceLibraryViewController = [[UIReferenceLibraryViewController alloc] initWithTerm:text];
    [self.navigationController pushViewController:referenceLibraryViewController animated:NO];
}

- (RACSignal *)suggestSiganl {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSRange range = NSMakeRange(0, self.searchBar.text.length);
        // NSArray *guesses = [self.textChecker guessesForWordRange:range inString:self.searchBar.text language:@"en_US"];
        NSArray *guesses = [[NSArray alloc] init];
        NSArray *completions = [self.textChecker completionsForPartialWordRange:range inString:self.searchBar.text language:@"en_US"];
        NSArray  *suggestes = [completions arrayByAddingObjectsFromArray:guesses];
        NSArray *result = [[[suggestes rac_sequence]  filter:^BOOL(NSString *value) {
            // return [UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:value];
            return YES;
        }] array];
        [subscriber sendNext:result];
        [subscriber sendCompleted];
        return nil;
    }];
}

#pragma mark - table view  delegate & datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.words count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [self.words objectAtIndex:indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

@end
