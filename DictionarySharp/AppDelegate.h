//
//  AppDelegate.h
//  DictionarySharp
//
//  Created by ngn999 on 16/3/30.
//  Copyright © 2016年 ngn999. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

